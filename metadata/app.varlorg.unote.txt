Categories:Writing
License:GPLv3
Web Site:https://gitlab.com/Varlorg/uNote
Source Code:https://gitlab.com/Varlorg/uNote/tree/HEAD
Issue Tracker:https://gitlab.com/Varlorg/uNote/issues

Auto Name:uNote
Summary:Lightweight and minimalist notepad
Description:
uNote is a lightweight and minimalist notepad
.

Repo Type:git
Repo:https://gitlab.com/Varlorg/uNote.git

Build:1.0,1
    commit=9815b027
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0.1
Current Version Code:1
