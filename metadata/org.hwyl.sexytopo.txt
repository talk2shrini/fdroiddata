Categories:Science & Education
License:GPLv3
Web Site:
Source Code:https://github.com/richsmith/sexytopo
Issue Tracker:https://github.com/richsmith/sexytopo/issues

Auto Name:SexyTopo
Summary:Assist in cave surveying
Description:
Assists in cave surveying. It interfaces with an automated measuring device (the
Disto-X) and builds up an accurate model of the cave. It also provides a
sketching environment for the cave surveyor to fill out the details.
.

Repo Type:git
Repo:https://github.com/richsmith/sexytopo

Build:1.0.6,7
    commit=5b9c649d540ce9a3d2bac102128d58cb2bf9303b
    gradle=yes

Build:1.0.7,8
    commit=810d4cce76ab3273daea2f8e03794a7aab61a9c5
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.7
Current Version Code:8
