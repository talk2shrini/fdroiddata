Categories:System
License:Apache2
Web Site:https://github.com/daktak/androidpn-client/blob/HEAD/README.md
Source Code:https://github.com/daktak/androidpn-client
Issue Tracker:https://github.com/daktak/androidpn-client/issues

Auto Name:AndroidPN Client
Summary:Push Notificitaion Client
Description:
Android Push Notificitaion Client. Obtain server from
[http://sourceforge.net/projects/androidpn/ here].
.

Repo Type:git
Repo:https://github.com/daktak/androidpn-client

Build:0.5.6,20160303
    commit=v0.5.6-r2
    subdir=app
    gradle=yes
    rm=app/src/main/lib/asmack.*

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.5.6
Current Version Code:20160303
